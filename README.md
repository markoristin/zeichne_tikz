# Zeichne TIKZ #

Zeichne TIKZ is a semi-WYSIWYG graph drawing program that automatically produces succint TIKZ code that can be immediatly inserted into a latex file. 

## Installation ##
The master branch is always deployed at http://marko.ristin.net/zeichne_tikz. 

If you want to use the software off-line, just clone the files somewhere on the disk and open ./index.html in your browser. 

We tested with Firefox and Chrome, but probably works with other browsers as well.

## Screenshot ##
![screenshot](https://bytebucket.org/markoristin/zeichne_tikz/raw/master/screenshot.png)