var Node = function() {
  var that = {};

  that.x = 0;
  that.y = 0;

  that.id = 'not_specified'
  that.text = '';

  that.highlighted = false;

  that.shapes = {
    container: null,
    text: null,
    shape: null
  };

  return that;
};

var Edge = function() {
  var that = {};

  that.source_node = null;
  that.target_node = null;

  that.text = '';

  // angle from source to target node
  that.radian = 0;
  that.degree = 0;

  that.highlighted = false;

  that.directed = false;

  that.shapes = {
    line: null,
    arrow_head: null,
    text: null
  };

  return that;
};

var Tool = function(app) {
  var that = {};

  that.app = app;

  that.init = function() {}

  that.select = function() {}
  that.unselect = function() {}

  that.on_node_mouseover = function(evt, node) {}
  that.on_node_mouseout = function(evt, node) {}
  that.on_node_pressmove = function(evt, node) {}
  that.on_node_pressup = function(evt, node) {}
  that.on_node_click = function(evt, node) {}
  that.on_node_dblclick = function(evt, node) {}

  that.on_edge_mouseover = function(evt, edge) {}
  that.on_edge_mouseout = function(evt, edge) {}
  that.on_edge_click = function(evt, edge) {}
  that.on_edge_dblclick = function(evt, edge) {}

  that.on_empty_click = function(evt) {}

  that.on_stage_move = function(evt) {}

  that.find_targets = function(evt) {
    var x = that.app.stage.mouseX;
    var y = that.app.stage.mouseY;
    var targets = that.app.stage.getObjectsUnderPoint(x,y);
  
    var target_node = null;
    var target_edge = null;

    $.each(targets, function(target_i, target) {
      $.each(that.app.nodes, function(node_i, node) {
        if(target_node === null && target === node.shapes.shape) {
          target_node = node;
        }
      });

      $.each(that.app.edges, function(edge_i, edge) {
        if(target_edge === null && 
           (target === edge.shapes.line || 
            (edge.directed && target == edge.shapes.arrow_head))) {
          target_edge = edge;
        }
      });
    });

    return {target_node: target_node, target_edge: target_edge};
  }

  // propagates the click to edge or a node,
  // on_node_click is not wired to 'click' event!
  that.on_stage_down = function(evt) {
    if(that.app.stage.mouseInBounds) {
      var targets = that.find_targets(evt);
      var target_node = targets.target_node;
      var target_edge = targets.target_edge;

      if(target_node !== null) {
        that.on_node_click(evt, target_node);
      }

      if(target_edge !== null) {
        that.on_edge_click(evt, target_edge);
      }

      if(target_node === null && target_edge === null) {
        that.on_empty_click(evt);
      }
    }
  }

  that.on_stage_dblclick = function(evt) {
    if(that.app.stage.mouseInBounds) {
      var targets = that.find_targets(evt);
      var target_node = targets.target_node;
      var target_edge = targets.target_edge;

      if(target_node !== null) {
        that.on_node_dblclick(evt, target_node);
      }

      if(target_edge !== null) {
        that.on_edge_dblclick(evt, target_edge);
      }

      if(target_node === null && target_edge === null) {
        that.on_empty_dblclick(evt);
      }
    }
  }

  return that;
};

var Create_tool = function(app) {
  var that = Tool(app);

  that.pointer = null;

  that.select = function() {
    if(that.pointer === null) {
      that.pointer = new createjs.Shape();
      that.pointer.graphics.beginFill("#000000").drawCircle(0, 0, that.app.radius);
      that.app.stage.addChild(that.pointer);
      that.app.stage.update();
    }

    $('#create_btn').css('color','blue');
  };

  that.unselect = function() {
    $('#create_btn').css('color','black');
    that.app.stage.removeChild(that.pointer);
    that.app.stage.update();
    that.pointer = null;
  }

  that.on_stage_move = function(evt) {
    if(that.app.stage.mouseInBounds) {
      that.pointer.x = evt.stageX;
      that.pointer.y = evt.stageY;
      that.app.stage.update();
    }
  };

  that.on_empty_click = function(evt) {
    that.app.create_node(evt.stageX, evt.stageY);
  };

  return that;
};

var Edge_tool = function(app) {
  var that = Tool(app);

  that.source_node = null;
  that.line = null;

  that.select = function() {
    $('#edge_btn').css('color','blue');
    that.source_node = null;
  };

  that.unselect = function() {
    $('#edge_btn').css('color','black');
    that.reset();
  }

  that.reset = function() {
    that.source_node = null;

    if(that.line !== null) {
      that.app.stage.removeChild(that.line);
      that.line = null;
      that.app.stage.update();
    }
  }

  that.on_stage_move = function(evt) {
    if(that.source_node !== null) {
      that.line.graphics.clear()
        .s('#f00')
        .mt(that.source_node.x, that.source_node.y)
        .lt(evt.stageX, evt.stageY);

      that.app.stage.update();
    }
  }

  that.on_node_mouseover = function(evt, node) {
    document.body.style.cursor='pointer';
  }

  that.on_node_mouseout = function(evt, node) {
    document.body.style.cursor='default';
  }

  that.on_node_click = function(evt, node) {
    if(that.source_node === null) {
      that.source_node = node;
      that.line = new createjs.Shape();
      that.app.stage.addChild(that.line);
    } else {
      that.app.create_edge(that.source_node, node);
      that.reset();
    }
  }

  that.on_empty_click = function(evt) {
    that.reset();
  }

  return that;
};

var Edit_tool = function(app) {
  var that = Tool(app);

  that.selected_node = null;
  that.selected_edge = null;

  that.init = function() {
    $('#delete_node_btn').click(function() {
      that.delete_selected_node();
    });

    $('#node_text').keyup(function(evt) {
      if(evt.keyCode == 27) {
        $('#node_text').blur();
      } else {
        var node_text = $('#node_text').val();
        that.app.set_node_text(that.selected_node, $('#node_text').val());
      }
    });

    $('#directed_cbox').change(function() {
      that.selected_edge.directed = $('#directed_cbox').prop('checked');
      that.app.redraw_edge(that.selected_edge);
      that.app.update();
    });

    $('#reverse_edge_btn').click(function() {
      that.app.reverse_edge(that.selected_edge);
    });

    $('#edge_text').keyup(function(evt) {
      if(evt.keyCode == 27) {
        $('#edge_text').blur();
      } else {
        var edge_text = $('#edge_text').val();
        that.selected_edge.text = edge_text;
        that.app.redraw_edge(that.selected_edge);
        that.app.update();
      }
    });

    $('#delete_edge_btn').click(function() {
      that.delete_selected_edge();
    });
  }

  that.select = function() {
    $('#edit_btn').css('color','blue');
    that.selected_node = null;
    that.selected_edge = null;

     $(document).bind('keyup.editTool', 'd', function() {
       that.delete_selected_node();
       that.delete_selected_edge();
       return false;
     });
  };

  that.unselect = function() {
    $('#edit_btn').css('color','black');

    that.unselect_node();
    that.unselect_edge();

    $(document).unbind('keyup.editTool');
  }

  that.unselect_node = function() {
    if(that.selected_node !== null) {
      that.app.unhighlight_node(that.selected_node);
      that.selected_node = null;
      $('#node_div').hide();

      $(document).unbind('keyup.editTool.node');
    }
  }

  that.unselect_edge = function() {
    if(that.selected_edge !== null) {
      that.app.unhighlight_edge(that.selected_edge);
      that.selected_edge = null;
      $('#edge_div').hide();
      $(document).unbind('keyup.editTool.edge');
    }
  }

  that.select_node = function(node) {
    if(node !== that.selected_node) {
      that.unselect_node();
      that.unselect_edge();

      that.selected_node = node;
      that.app.highlight_node(that.selected_node);
      $('#node_div').show();
      $('#node_text').val(node.text);

      $(document).bind('keyup.editTool.node', 'x', function() {
        $('#node_text').focus().select();
      });
    }
  }


  that.on_node_click = function(evt, node) {
    that.select_node(node);
  }

  that.on_node_dblclick = function(evt, node) {
    $('#node_text').focus().select();
  }

  that.on_node_pressmove = function(evt, node) {
    that.select_node(node);
    document.body.style.cursor='move';
    that.app.move_node(that.selected_node, evt.stageX, evt.stageY);
  }

  that.on_node_pressup = function(evt, node) {
    document.body.style.cursor='default';
  }

  that.on_edge_click = function(evt, edge) {
    that.unselect_node();
    that.unselect_edge();

    that.selected_edge = edge;
    that.app.highlight_edge(that.selected_edge);
    $('#edge_div').show();
    $('#edge_text').val(edge.text);
    $('#directed_cbox').prop('checked', that.selected_edge.directed);
    $('#edge_text').val(edge.text);

    $(document).bind('keyup.editTool.edge', 'i', function() {
      that.selected_edge.directed = !that.selected_edge.directed;
      $('#directed_cbox').prop('checked', that.selected_edge.directed);
      that.app.redraw_edge(that.selected_edge);
    });

    $(document).bind('keyup.editTool.edge', 'x', function() {
      $('#edge_text').focus().select();
    });

    $(document).bind('keyup.editTool.edge', 'r', function() {
      that.app.reverse_edge(that.selected_edge);
    });
  }

  that.on_edge_dblclick = function(evt, edge) {
    $('#edge_text').focus().select();
  }

  that.on_node_mouseover = function(evt, node) {
    document.body.style.cursor='pointer';
  }

  that.on_node_mouseout = function(evt, node) {
    document.body.style.cursor='default';
  }

  that.on_edge_mouseover = function(evt, edge) {
    document.body.style.cursor='pointer';
  }

  that.on_edge_mouseout = function(evt, edge) {
    document.body.style.cursor='default';
  }

  that.on_empty_click = function(evt) {
    that.unselect_node();
    that.unselect_edge();
  }

  that.delete_selected_node = function() {
    if(that.selected_node !== null) {
      that.app.delete_node(that.selected_node);
      that.selected_node = null;
      $('#node_div').hide();
    }
  }

  that.delete_selected_edge = function() {
    if(that.selected_edge !== null) {
      that.app.delete_edge(that.selected_edge);
      that.selected_edge = null;
      $('#edge_div').hide();
    }
  }

  return that;
}

var zeichne_tikz = (function() {
  var that={};

  that.nodes = [];
  that.edges = [];

  that.next_node_id = 1;

  that.radius = 10;
  that.dpi = 72;

  that.stage = null;

  that.create_tool = null;
  that.edge_tool = null;
  that.edit_tool = null;

  that.tool_pool = [];

  that.tool = null;

  that.node_highlight_color = "#98AFC7";
  that.node_normal_color = "#C7B097";

  that.edge_highlight_color = "#98AFC7";
  that.edge_normal_color = "#C7B097";

  that.grid_size = 10;
  that.grid_shapes = [];
  that.grid_color = "#d3d3d3";

  that.alphabet = [
    'A','B','C','D','E','F','G','H','I','J','K','L',
    'M','N','O','P','Q','R','S','T','U','V','W','X',
    'Y','Z'
  ];


  that.init = function() {
    that.stage = new createjs.Stage("my_canvas");
    that.stage.enableMouseOver(20);

    that.redraw_grid();
  }

  that.select_tool = function(tool) {
    if(that.tool !== null) {
      that.tool.unselect();
    }
    that.tool = tool;
    tool.select();
  }

  that.init_tools = function() {
    that.create_tool = Create_tool(that);
    that.edge_tool = Edge_tool(that);
    that.edit_tool = Edit_tool(that);

    that.tool_pool = [
      that.create_tool,
      that.edge_tool,
      that.edit_tool
    ];

    $.each(that.tool_pool, function(tool_i, tool) {
      tool.init();
    });

    $('#clear_btn').click(function() {
      that.clear();
      that.select_tool(that.create_tool);
    });

    $('#create_btn').click(function() {
      that.select_tool(that.create_tool);
    });

    $('#edge_btn').click(function() {
      that.select_tool(that.edge_tool);
    });

    $('#edit_btn').click(function() {
      that.select_tool(that.edit_tool);
    });

    $('#direct_btn').click(function() {
      that.set_directed_for_all_edges(true);
    });

    $('#undirect_btn').click(function() {
      that.set_directed_for_all_edges(false);
    });

    $('#alphabetize_btn').click(function() {
      that.alphabetize_nodes();
    });

    $('#enumerate_btn').click(function() {
      that.enumerate_nodes();
    });

    that.select_tool(that.create_tool);

     $(document).bind('keyup', 'c', function() {
       that.select_tool(that.create_tool);
       return false;
     });

     $(document).bind('keyup', 'e', function() {
       that.select_tool(that.edge_tool);
       return false;
     });

     $(document).bind('keyup', 't', function() {
       that.select_tool(that.edit_tool);
       return false;
     });

    that.stage.on("stagemousemove", function(evt) {
      that.tool.on_stage_move(evt);
    });

    that.stage.on("stagemousedown", function(evt) {
      that.tool.on_stage_down(evt);
    });

    that.stage.on("dblclick", function(evt) {
      that.tool.on_stage_dblclick(evt);
    });
  }

  that.clear = function() {
    that.next_node_id = 1;

    $.each(that.nodes, function(node_i, node) {
      that.stage.removeChild(node.shapes.shape);
      that.stage.removeChild(node.shapes.text);
      that.stage.removeChild(node.shapes.container);
    });

    $.each(that.edges, function(edge_i, edge) {
      that.stage.removeChild(edge.shapes.line);

      if(edge.shapes.arrow_head !== null) {
        that.stage.removeChild(edge.shapes.arrow_head);
      }

      if(edge.shapes.text !== null) {
        that.stage.removeChild(edge.shapes.text);
      }
    });

    that.nodes = [];
    that.edges = [];

    that.update();
  };

  that.create_node = function(x,y) {
    var node = Node();

    node.x = x;
    node.y = y;

    node.shapes.container = new createjs.Container();
    node.shapes.container.x = x;
    node.shapes.container.y = y;
    node.shapes.container.setBounds(0, 0, 2* that.radius, 2 *that.radius);

    node.shapes.shape = new createjs.Shape();
    node.shapes.shape.graphics
      .beginFill(that.node_normal_color)
      .drawCircle(0, 0, that.radius);

    node.shapes.container.addChild(node.shapes.shape);

    node.id = that.next_node_id;
    that.next_node_id++;

    node.text = sprintf('%d',node.id);

    node.shapes.text = new createjs.Text();
    node.shapes.text.set({
      text: node.text,
      textAlign: 'center'
    });
    node.shapes.container.addChild(node.shapes.text);

    node.shapes.shape.on('mouseover', (function(a_node) {
      return function(evt) {
        that.tool.on_node_mouseover(evt, a_node);
      }; 
    }(node)));

    node.shapes.shape.on('mouseout', (function(a_node) {
      return function(evt) {
        that.tool.on_node_mouseout(evt, a_node);
      }; 
    }(node)));

    node.shapes.shape.on('pressmove', (function(a_node) {
      return function(evt) {
        that.tool.on_node_pressmove(evt, a_node);
      }; 
    }(node)));

    node.shapes.shape.on('pressup', (function(a_node) {
      return function(evt) {
        that.tool.on_node_pressup(evt, a_node);
      }; 
    }(node)));

    that.stage.addChild( node.shapes.container);

    that.nodes.push(node);

    that.update();
  }

  that.highlight_node = function(node) {
    if(node.highlighted === false) {
      node.shapes.shape.graphics.clear()
        .beginFill(that.node_highlight_color)
        .drawCircle(0, 0, that.radius);
      that.stage.update();

      node.highlighted = true;
    }
  };

  that.unhighlight_node = function(node) {
    if(node.highlighted === true) {
      node.shapes.shape.graphics.clear();
      node.shapes.shape.graphics.clear()
        .beginFill(that.node_normal_color)
        .drawCircle(0, 0, that.radius);
      that.stage.update();

      node.highlighted = false;
    }
  };

  that.move_node = function(node, x, y) {
    node.x = x;
    node.y = y;
    node.shapes.container.x = x;
    node.shapes.container.y = y;

    $.each(that.edges, function(edge_i, edge) {
      if(edge.source_node === node || edge.target_node === node) {
        that.redraw_edge(edge);
      }
    });

    that.update();
  }

  that.set_node_text = function(node, text) {
    node.text = text;
    node.shapes.text.text = text;
    that.update();
  }

  that.delete_node = function(node) {
    that.stage.removeChild(node.shapes.container);
    node.shapes.container = null;
    node.shapes.shape = null;
    node.shapes.text = null;

    var morituri = that.nodes.indexOf(node);

    that.nodes.splice(morituri, 1);

    var affected_edges = [];
    $.each(that.edges, function(edge_i, edge) {
      if(edge.source_node === node || edge.target_node) {
        affected_edges.push(edge);
      }
    });

    $.each(affected_edges, function(edge_i, edge) {
      that.delete_edge(edge);
    });

    that.update();
  }

  that.create_edge = function(source_node, target_node) {
    var edge = Edge();

    edge.source_node = source_node;
    edge.target_node = target_node;
    that.redraw_edge(edge);

    that.edges.push(edge);

    edge.shapes.line.on('mouseover', (function(a_edge) {
      return function(evt) {
        that.tool.on_edge_mouseover(evt, a_edge);
      }; 
    }(edge)));

    edge.shapes.line.on('mouseout', (function(a_edge) {
      return function(evt) {
        that.tool.on_edge_mouseout(evt, a_edge);
      }; 
    }(edge)));

    that.update();
  }

  that.delete_edge = function(edge) {
    that.stage.removeChild(edge.shapes.line);

    if(edge.shapes.arrow_head !== null) {
      that.stage.removeChild(edge.shapes.arrow_head);
    }

    if(edge.shapes.text !== null) {
      that.stage.removeChild(edge.shapes.text);
    }

    edge.shapes.line = null;
    edge.shapes.arrow_head = null;
    edge.shapes.text = null;

    var morituri = that.edges.indexOf(edge);
    that.edges.splice(morituri, 1);

    that.update();
  }

  that.highlight_edge = function(edge) {
    if(!edge.highlighted){
      edge.highlighted = true;
      that.redraw_edge(edge);
    }
  }

  that.unhighlight_edge = function(edge) {
    if(edge.highlighted){
      edge.highlighted = false;
      that.redraw_edge(edge);
    }
  }

  that.reverse_edge = function(edge) {
    var tmp_node = edge.source_node;
    edge.source_node = edge.target_node;
    edge.target_node = tmp_node;
    that.redraw_edge(edge);
  }

  that.redraw_edge = function(edge) {
    if(edge.shapes.line === null) {
      edge.shapes.line = new createjs.Shape();
      that.stage.addChild(edge.shapes.line);
    }

    var color = that.edge_normal_color;
    if(edge.highlighted){
      color=that.edge_highlight_color;
    }

    edge.radian = Math.atan2(edge.target_node.y - edge.source_node.y,
                             edge.target_node.x - edge.source_node.x);

    edge.degree = edge.radian / Math.PI * 180;

    var src_x = edge.source_node.x + that.radius * Math.cos(edge.radian);
    var src_y = edge.source_node.y + that.radius * Math.sin(edge.radian);

    var tgt_x = edge.target_node.x - that.radius * Math.cos(edge.radian);
    var tgt_y = edge.target_node.y - that.radius * Math.sin(edge.radian);

    edge.shapes.line.graphics.clear()
      .setStrokeStyle(4)
      .beginStroke(color)
      .moveTo(src_x, src_y)
      .lineTo(tgt_x, tgt_y);

    if(edge.text !== '') {
      var dx = tgt_x - src_x;
      var dy = tgt_y - src_y;
      var length = Math.sqrt( dy * dy + dx * dx );

      dx /= length;
      dy /= length;

      var x = src_x + dx * length / 2;
      var y = src_y + dy * length / 2;

      if(edge.shapes.text === null) {
        edge.shapes.text = new createjs.Text();
        that.stage.addChild(edge.shapes.text);
      }

      edge.shapes.text.set({
        x: x,
        y: y,
        text: edge.text
      });
    } else {
      if(edge.shapes.text !== null) {
        that.stage.removeChild(edge.shapes.text);
        edge.shapes.text = null;
      }
    }

    if(edge.directed === true) {
      if(edge.shapes.arrow_head === null) {
        edge.shapes.arrow_head = new createjs.Shape();
        that.stage.addChild(edge.shapes.arrow_head);
      }

      edge.shapes.arrow_head.graphics.clear()
        .setStrokeStyle(4)
        .beginStroke(color)
        .moveTo(-5, +5)
        .lineTo(0, 0)
        .lineTo(-5, -5);
      
      edge.shapes.arrow_head.x = tgt_x;
      edge.shapes.arrow_head.y = tgt_y;
      edge.shapes.arrow_head.rotation = edge.degree;
    } else {
      if(edge.shapes.arrow_head !== null) {
        that.stage.removeChild(edge.shapes.arrow_head);
        edge.shapes.arrow_head = null;
      }
    }

    that.stage.update();
  }

  that.redraw_grid = function() {
    return;
    $.each(that.grid_shapes, function(line_i, line) {
      that.stage.removeChild(line);
    });

    var background_index = that.stage.getNumChildren();

    var width = that.stage.canvas.width;
    var height = that.stage.canvas.height;

    for(var y = 0; y < height; y += that.grid_size) {
      var line = new createjs.Shape();
      line.graphics
        .setStrokeStyle(1)
        .beginStroke(that.grid_color)
        .moveTo(0, y)
        .lineTo(width - 1, y);

      that.stage.addChild(line);
      that.stage.setChildIndex(line, background_index);
      that.stage.update();

      that.grid_shapes.push(line);
    }

    for(var x = 0; x < width; x += that.grid_size) {
      var line = new createjs.Shape();
      line.graphics
        .setStrokeStyle(1)
        .beginStroke(that.grid_color)
        .moveTo(x, 0)
        .lineTo(x, height - 1);

      that.stage.addChild(line);
      that.stage.setChildIndex(line, background_index);
      that.stage.update();

      that.grid_shapes.push(line);
    }
  }

  that.set_directed_for_all_edges = function(directed) {
    $.each(that.edges, function(edge_i, edge) {
      edge.directed = directed;
      that.redraw_edge(edge);
    });

    that.update();
    that.select_tool(that.tool);
  }

  that.alphabetize_nodes = function() {
    if(that.nodes.length > that.alphabet.length) {
      console.error('not implemented.');
    } else {
      $.each(that.nodes, function(node_i, node) {
        var letter = that.alphabet[node_i];
        node.id = letter;
        that.set_node_text(node, letter);
      });
    }
  }

  that.enumerate_nodes = function() {
    var id = 1;
    $.each(that.nodes, function(node_i, node) {
      node.id = id;
      that.set_node_text(node, sprintf('%d', node.id));
      id += 1;
    });
  }

  that.update = function() {
    that.stage.update();
    that.update_code();
  }

  that.update_code = function() {
    var code = [];
    code.push('\\begin{tikzpicture}');
    code.push('  \\tikzstyle{place}=[rectangle,thick,'+
              'draw=blue!75,fill=blue!20,minimum size=6mm]');

    if(that.nodes.length > 0) {
      var min_x = that.nodes[0].x;
      var min_y = that.nodes[0].y;

      $.each(that.nodes, function(node_i, node) {
        min_x = (node.x < min_x) ? node.x : min_x;
        min_y = (node.y < min_y) ? node.y : min_y;
      });

      $.each(that.nodes, function(node_i, node) {
        var xshift = ((node.x - min_x) / that.dpi) * 2.54;
        var yshift = -((node.y - min_y) / that.dpi) * 2.54;

        code.push(sprintf(
          '  \\node[place] (%s) [xshift=%.2fcm, yshift=%.2fcm] {%s};',
          node.id, xshift, yshift, node.text));
      });

      $.each(that.edges, function(edge_i, edge) {
        var arrow_code = (edge.directed === false) ? '' : '[-latex]';


        if(edge.text === '') {
          code.push(sprintf('  \\draw %s (%s) -> (%s);',
            arrow_code, edge.source_node.id, edge.target_node.id ));
        } else {
          code.push(sprintf(
            '  \\draw %s (%s) -> (%s) node [ midway, fill=white] {%s};',
            arrow_code, edge.source_node.id, edge.target_node.id,
            edge.text));
        }
      });
    }

    code.push('\\end{tikzpicture}');

    $('#code_pre').html(code.join('\n'));
  };

  return that;
})();

$(document).ready(function() {
  zeichne_tikz.init();
  zeichne_tikz.init_tools();
});
